import copy
import random

from errors import FeatureDoesNotExistForVariableError, FeatureValueOutOfRangeError, \
    NotEnoughVariableFeatureDuosError, MultiNumValuesFeaturesPassedToIdentityStatementError, \
    VariableFeatureDuoAlreadyExistsInIdentityStatementError, VariableFeatureDuoNotInIdentityStatementError


class FeatureStatement(object):
    def change(self, **kwargs):
        raise NotImplementedError()

    def is_respected_by(self, features_dict):
        raise NotImplementedError()


class VariableFeatureDuo(object):
    def __init__(self, variable, feature, force=False):
        if not force:
            if not variable.has_feature(feature):
                raise FeatureDoesNotExistForVariableError()

        self.variable = variable
        self.feature = feature

    def __eq__(self, other):
        return type(self) == type(other) and self.variable == other.variable and self.feature == other.feature

    def __hash__(self):
        return hash((self.variable, self.feature))

    def __repr__(self):
        return '{}{{{}}}'.format(self.variable.name, self.feature)


class FeatureValueSetStatement(FeatureStatement):
    def __init__(self, variable_feature_duo, value=None):
        self.variable_feature_duo = variable_feature_duo

        if not value:
            value = random.choice(
                self.variable_feature_duo.feature.values)
        elif value not in self.variable_feature_duo.feature.values:
            raise FeatureValueOutOfRangeError()

        self.value = value

    def get_variable_feature_duo(self):
        return self.variable_feature_duo

    def __eq__(self, other):
        return type(self) == type(
            other) and self.variable_feature_duo == other.variable_feature_duo and self.value == other.value

    def equivalent_to(self, other):
        return self.variable_feature_duo == other.variable_feature_duo

    def change(self, **kwargs):
        self.change_value()

    def change_value(self):
        while True:
            value = random.choice(
                self.variable_feature_duo.feature.values)
            if value != self.value:
                self.value = value
                break

    def is_respected_by(self, features_dict):
        if self.variable_feature_duo.feature not in features_dict:
            return True  # vacuously
        return features_dict[self.variable_feature_duo.feature] == self.value


class FeatureValuesIdentityStatement(FeatureStatement):
    MIN_NUM_DUOS = 2

    def __init__(self, variable_feature_duos):
        if len(variable_feature_duos) < FeatureValuesIdentityStatement.MIN_NUM_DUOS:
            raise NotEnoughVariableFeatureDuosError()

        nums_values = {duo.feature.num_values for duo in variable_feature_duos}
        if len(nums_values) != 1:
            raise MultiNumValuesFeaturesPassedToIdentityStatementError()

        self.variable_feature_duos = copy.copy(variable_feature_duos)
        self.num_values_in_features = nums_values.pop()

    def change(self, available_variable_feature_duos_by_variable=None):
        if not available_variable_feature_duos_by_variable:
            available_variable_feature_duos_by_variable = {}

        should_add = False
        should_remove = False

        duos_to_possibly_add = set().union(
            *[available_variable_feature_duos_by_variable[var] for var in available_variable_feature_duos_by_variable if
              not self.has_variable(var)])
        duos_to_possibly_add = {duo for duo in duos_to_possibly_add if
                                duo.feature.num_values == self.num_values_in_features}

        if len(duos_to_possibly_add) == 0:
            should_remove = True
        elif len(self.variable_feature_duos) == FeatureValuesIdentityStatement.MIN_NUM_DUOS:
            should_add = True
        else:
            if random.random() < 0.5:
                assert len(self.variable_feature_duos) > FeatureValuesIdentityStatement.MIN_NUM_DUOS
                should_remove = True
            else:
                should_add = True

        if should_add:
            duo_to_add = random.choice(list(duos_to_possibly_add))
            self.add(duo_to_add)

        if should_remove:
            self.remove_some_variable_feature_duo()

    def add(self, variable_feature_duo):
        if self.contains(variable_feature_duo):
            raise VariableFeatureDuoAlreadyExistsInIdentityStatementError()

        self.variable_feature_duos.add(variable_feature_duo)

    def __len__(self):
        return len(self.variable_feature_duos)

    def contains(self, variable_feature_duo):
        return variable_feature_duo in self.variable_feature_duos

    def has_variable(self, variable):
        for variable_feature_duo in self.variable_feature_duos:
            if variable == variable_feature_duo.variable:
                return True
        return False

    def remove(self, variable_feature_duo):
        if not self.contains(variable_feature_duo):
            raise VariableFeatureDuoNotInIdentityStatementError()

        if len(self) == FeatureValuesIdentityStatement.MIN_NUM_DUOS:
            raise NotEnoughVariableFeatureDuosError()

        self.variable_feature_duos.remove(variable_feature_duo)

    def remove_some_variable_feature_duo(self):
        self.remove(random.choice(list(self.variable_feature_duos)))

    def split(self):
        if len(self) < 2 * FeatureValuesIdentityStatement.MIN_NUM_DUOS:
            raise NotEnoughVariableFeatureDuosError()

        num = random.choice(range(FeatureValuesIdentityStatement.MIN_NUM_DUOS,
                                  len(self) - FeatureValuesIdentityStatement.MIN_NUM_DUOS + 1))
        random_order_list = list(self.variable_feature_duos)
        random.shuffle(random_order_list)
        return FeatureValuesIdentityStatement(set(random_order_list[:num])), \
               FeatureValuesIdentityStatement(set(random_order_list[num:]))

    @staticmethod
    def merge_identity_statements(statements):
        assert all([type(statement) == FeatureValuesIdentityStatement for statement in
                    statements]), 'all statements passed to merge_identity_statements should be Identity statements'

        variable_feature_duos = set().union(*[statement.variable_feature_duos for statement in statements])
        return FeatureValuesIdentityStatement(variable_feature_duos)

    @property
    def features(self):
        return [duo.feature for duo in self.variable_feature_duos]

    def is_respected_by(self, features_dict):
        return len({features_dict[feature] for feature in features_dict if feature in self.features}) <= 1
