import random

from errors import NoVariableFeaturesError, ReachedMaxNumOfVariableFeaturesError, FeatureDoesNotExistForVariableError, \
    PrototypeForRuleVariableShouldBeAVariableError
from feature import Feature


class Variable(object):
    BASE_FEATURE_NAME = 'f{}'
    MAX_NUM_FEATURES = 3

    def __init__(self, name, features=None):
        if not features:
            features = {}

        self.name = name
        self.features = features
        self.instances = []

        self.id_str = None

    def __repr__(self):
        features_string = '{{{}}}'.format(
            ' '.join(['{}:{}'.format(feature, self.features[feature]) for feature in self.features]))
        return '{} {}'.format(self.name, features_string)

    def get_feature_position(self, feature):
        return list(self.features.keys()).index(feature)

    @property
    def cost_str(self):
        '''
        :return: cost_str: <f1_num_values_5_bits><f2_num_values_5_bits>...<fn_num_values_5_bits>00000
        '''
        num_values_per_feature_str = ''.join(
            [bin(feature.num_values)[2:].zfill(5) for feature in self.features] + ['00000'])
        return num_values_per_feature_str

    @property
    def cost(self):
        return len(self.cost_str)

    def get_new_feature_name(self):
        i = 1
        while True:
            feature_name = Variable.BASE_FEATURE_NAME.format(i)
            if feature_name not in [feature.name for feature in self.features]:
                return feature_name
            i += 1

    def add_some_new_feature(self):
        new_feature_name = self.get_new_feature_name()
        new_feature = Feature(new_feature_name)
        self.add_feature(new_feature)
        for instance in self.instances:
            instance.add_feature(new_feature)
        return new_feature

    def remove_some_feature(self):
        if len(self.features) == 0:
            raise NoVariableFeaturesError()
        feature = random.choice(list(self.features))
        self.remove_feature(feature)
        for instance in self.instances:
            instance.remove_feature(feature)
        return feature

    def add_feature(self, feature):
        if len(self.features) == Variable.MAX_NUM_FEATURES:
            raise ReachedMaxNumOfVariableFeaturesError()
        self.features[feature] = ''

    def has_feature(self, feature):
        return feature in self.features

    def remove_feature(self, feature):
        if feature not in self.features:
            raise FeatureDoesNotExistForVariableError()
        self.features.pop(feature)

    def set_feature_value(self, feature, value):
        if feature not in self.features:
            raise FeatureDoesNotExistForVariableError()
        assert (type(value) == str and (value == '' or value.startswith('Z') or value.startswith('x'))) or (type(value) == int and value in feature.values)
        self.features[feature] = value

    def add_instance(self, instance):
        self.instances.append(instance)


class LexicalVariable(Variable):
    def __init__(self, name, features=None):
        super().__init__(name, features)
        self.lexical_items = []

    def add_lexical_item(self, lexical_item):
        self.lexical_items.append(lexical_item)

    def remove_all_lexical_items(self):
        self.lexical_items = []

    def get_num_lexical_entries_by_features(self, features):
        assert all(feature in self.features for feature in features)
        return sum(li.get_num_instances_by_features(features) for li in self.lexical_items)

    def get_lexical_instances_by_features(self, features):
        assert all(feature in self.features for feature in features)
        all_relevant_instances = []
        for li in self.lexical_items:
            all_relevant_instances.extend([(li.word, instance) for instance in li.get_instances_by_features(features)])
        return all_relevant_instances

    @property
    def cost_str(self):
        '''
        :return: cost_str:  <f1_num_values_5_bits><f2_num_values_5_bits>...<fn_num_values_5_bits>00000 --->
                            <word1_str><word2_str>...<word_n_str>
        '''
        return super().cost_str + ''.join([word.cost_str for word in self.lexical_items])

    @property
    def cost(self):
        return len(self.cost_str)


class VariableInstance(Variable):  # for use in rules (Rule class is to create a variable prototype)
    def __init__(self, prototype):
        if not isinstance(prototype, Variable) or type(prototype) == VariableInstance:
            raise PrototypeForRuleVariableShouldBeAVariableError()

        super().__init__(prototype.name, dict(prototype.features))

        prototype.add_instance(self)
        self.prototype = prototype
