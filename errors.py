class MorphologicalLearnerError(Exception):
    pass


class SetFeatureValueStatementsValuesClashError(MorphologicalLearnerError):
    pass


class FeatureDoesNotExistForVariableError(MorphologicalLearnerError):
    pass


class FeatureExistsForVariableError(MorphologicalLearnerError):
    pass


class ReachedMaxNumOfVariableFeaturesError(MorphologicalLearnerError):
    pass


class NoVariableFeaturesError(MorphologicalLearnerError):
    pass


class PrototypeForRuleVariableShouldBeAVariableError(MorphologicalLearnerError):
    pass


class FeatureValueOutOfRangeError(MorphologicalLearnerError):
    pass


class VariableFeatureDuoAlreadyExistsInIdentityStatementError(MorphologicalLearnerError):
    pass


class VariableFeatureDuoNotInIdentityStatementError(MorphologicalLearnerError):
    pass


class NotEnoughVariableFeatureDuosError(MorphologicalLearnerError):
    pass


class MultiNumValuesFeaturesPassedToIdentityStatementError(MorphologicalLearnerError):
    pass


class ClashingStatementsError(MorphologicalLearnerError):
    pass


class NoChangeOfStatementSoughtError(MorphologicalLearnerError):
    pass


class StatementAlreadyExistsError(MorphologicalLearnerError):
    pass


class EquivalentStatementAlreadyExistsError(MorphologicalLearnerError):
    pass


class StatementDoesNotExistError(MorphologicalLearnerError):
    pass


class IndexOfVariableOutOfRangeError(MorphologicalLearnerError):
    pass


class NotEnoughStatementsError(MorphologicalLearnerError):
    pass


class NoSuitableIdentityStatementToSplitError(MorphologicalLearnerError):
    pass


class CannotAddNewSetStatementError(MorphologicalLearnerError):
    pass


class CannotAddNewIdentityStatementError(MorphologicalLearnerError):
    pass


class ChangingRuleDidNotGoThroughError(MorphologicalLearnerError):
    pass


class CannotMergeAnyIdentityStatements(MorphologicalLearnerError):
    pass


class FeaturesMismatchError(MorphologicalLearnerError):
    pass


class CannotChangeInstanceInLexiconItem(MorphologicalLearnerError):
    pass


class CannotAddInstanceToLexiconItem(MorphologicalLearnerError):
    pass


class LexiconItemIsSaturatedError(MorphologicalLearnerError):
    pass


class TooLittleInstancesExistInLexiconItemError(MorphologicalLearnerError):
    pass


class StartVariableNotInVariableVIMapError(MorphologicalLearnerError):
    pass


class ChangingGrammarDidNotGoThroughError(MorphologicalLearnerError):
    pass


class NoFeaturesError(MorphologicalLearnerError):
    pass


class NoLexicalItemsForLexicalVariable(MorphologicalLearnerError):
    pass


class NoProperLexicalVariablesError(MorphologicalLearnerError):
    pass


class NoLexicalRuleToRemoveError(MorphologicalLearnerError):
    pass
