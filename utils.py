def split_word_to_n_parts(word, num_parts, start=0):
    if num_parts == 1:
        return [(word[start:],)]
    if len(word) == 0:
        return [('',) * num_parts]
    if start == len(word):
        return [('',) * num_parts]
    possible_indices = range(start, len(word) + 1)  # (0,1,2,3,4)
    answers = []
    for ind in possible_indices:
        part1 = (word[start:ind],)
        answers.extend([part1 + cont for cont in split_word_to_n_parts(word, num_parts - 1, ind)])
    return answers

