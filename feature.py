import random


class Feature(object):
    ALLOWED_NUM_VALUES = [2]

    def __init__(self, name, num_values=None):
        if not num_values:
            num_values = random.choice(Feature.ALLOWED_NUM_VALUES)

        self.name = name
        self.num_values = num_values

    def __repr__(self):
        return '{}-#{}'.format(self.name, self.num_values)

    @property
    def values(self):
        return range(self.num_values)
