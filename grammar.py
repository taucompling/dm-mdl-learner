import copy
import itertools
import random

from errors import StartVariableNotInVariableVIMapError, NoFeaturesError, NoLexicalRuleToRemoveError, \
    NoProperLexicalVariablesError, MorphologicalLearnerError, ChangingGrammarDidNotGoThroughError, \
    NoLexicalItemsForLexicalVariable, SetFeatureValueStatementsValuesClashError
from feature_statements import VariableFeatureDuo
from lexical_item import LexicalItem
from node import Node, Tree
from rule import LexicalRule, Rule
from simulated_annealer import ImproperNeighbourError
from utils import split_word_to_n_parts
from variable import LexicalVariable, Variable, VariableInstance

NUM_TRIES = 100


class Grammar(object):
    def __init__(self, variable_VI_map, start_variable, rules):
        self.variables = {}
        self.variable_name_to_variable_map = {}

        if start_variable not in variable_VI_map:
            raise StartVariableNotInVariableVIMapError()

        for variable_name in variable_VI_map:
            if len(variable_VI_map[variable_name]) == 0:
                variable = Variable(variable_name)
                self.variables[variable] = []
            else:
                variable = LexicalVariable(variable_name)
                self.variables[variable] = [LexicalItem(word, variable) for word in variable_VI_map[variable_name]]

            if variable_name == start_variable:
                self.start_variable = variable
                self.start_variable_name = start_variable

            self.variable_name_to_variable_map[variable_name] = variable

        self.rules = {var: [] for var in self.variables}
        for rule in rules:
            var_prototype = self.variable_name_to_variable_map[rule[0]]
            derivation = [VariableInstance(self.variable_name_to_variable_map[v]) for v in rule[1]]
            if type(var_prototype) == Variable:
                self.rules[var_prototype].append(Rule(VariableInstance(var_prototype), derivation))
            elif type(var_prototype) == LexicalVariable:
                raise NotImplementedError()

    @property
    def lexical_words(self):
        return {lex_item.word for lex_var in self.lexical_variables for lex_item in lex_var.lexical_items}

    def max_num_leaves_under_var(self, var):
        if len(self.rules[var]) == 0:
            return 1
        return max(1,
                   max(
                       sum(map(self.max_num_leaves_under_var, [v.prototype for v in rule.derivation]))
                       for rule in self.rules[var]))

    @property
    def lexical_variables(self):
        return [variable for variable in self.variables if type(variable) == LexicalVariable]

    @property
    def all_rules(self):
        return sum(self.rules.values(), [])

    @property
    def all_lexical_rules(self):
        return [rule for rule in self.all_rules if type(rule) == LexicalRule]

    @property
    def cost_str(self):
        # Lexicon
        variables_ordered = [var for var in self.variables if var != self.start_variable]
        variables_ordered = [self.start_variable] + variables_ordered
        for index, variable in enumerate(variables_ordered):
            variable.id_str = bin(index)[2:].zfill(len(bin(len(variables_ordered))[2:]))

        variables_str = '11110'.join([var.cost_str for var in variables_ordered] + [''])
        lexicon_str = '{}11111'.format(variables_str)

        rules_str = ''.join([rule.cost_str for rule in self.all_rules])

        return lexicon_str + rules_str

    def __repr__(self):
        variables_strs = []
        for variable in self.lexical_variables:
            variable_str = '{}:\t'.format(variable.name) + ', '.join(str(word) for word in self.variables[variable])
            variables_strs.append(variable_str)
        variables_str = '\n\t\t'.join(variables_strs)
        lexicon_str = 'Lexicon:\t{}'.format(variables_str)

        start_variable_str = 'Start Variable:\t{}'.format(self.start_variable_name)

        cs_str = 'Rules:\t{}'.format('\n\t'.join([str(rule) for rule in self.all_rules]))

        return '\n\n'.join([lexicon_str, start_variable_str, cs_str])

    def add_duo_to_lexicon(self, variable, feature):
        if variable in self.lexical_variables:
            for item in self.variables[variable]:
                item.add_new_feature(feature)

    def dispose_of_duo(self, variable, feature):
        if variable in self.lexical_variables:
            for item in self.variables[variable]:
                item.remove_feature(feature)

        for rule in self.all_rules:
            rule.remove_statements_with_variable_feature(variable, feature)

    @property
    def cost(self):
        return len(self.cost_str)

    def change_some_variable(self):
        variables_list = list(self.variables)
        variables_list.remove(self.start_variable)  # excluding start_variable
        chosen_variable = random.choice(variables_list)

        if random.random() < 0.5:
            feature = chosen_variable.add_some_new_feature()
            self.add_duo_to_lexicon(chosen_variable, feature)
        else:
            feature = chosen_variable.remove_some_feature()
            self.dispose_of_duo(chosen_variable, feature)

    def change_some_rule(self):
        chosen_rule = random.choice(self.all_rules)
        chosen_rule.change()

    def change_some_VI(self):
        variables_with_features = [var for var in self.lexical_variables if len(var.features) > 0]
        if len(variables_with_features) == 0:
            raise NoFeaturesError()

        chosen_var = random.choice(variables_with_features)
        items = list(self.variables[chosen_var])
        if len(items) == 0:
            raise NoLexicalItemsForLexicalVariable('A below-the-word-level rule must have been added...')

        item_to_change = random.choice(items)
        item_to_change.change()

    def remove_rule_below_word_level(self):
        rules_pool = [rule for rule in self.all_lexical_rules if all(type(var_instance.prototype) == LexicalVariable and
                                                                     len(var_instance.prototype.lexical_items) > 0 for
                                                                     var_instance in rule.derivation)]
        if len(rules_pool) == 0:
            raise NoLexicalRuleToRemoveError()

        chosen_rule = random.choice(rules_pool)
        assert len(chosen_rule.variable.prototype.lexical_items) == 0

        words_instances = {}

        v1, v2 = chosen_rule.derivation
        for v1_item in v1.prototype.lexical_items:
            for v2_item in v2.prototype.lexical_items:
                concatenated_word = v1_item.word + v2_item.word
                for v1_item_instance in v1_item.instances:
                    for v2_item_instance in v2_item.instances:
                        combined_features_dict = dict(v1_item_instance)
                        combined_features_dict.update(v2_item_instance)
                        assert len(combined_features_dict) == len(v1_item_instance) + len(v2_item_instance)
                        if all(statement.is_respected_by(combined_features_dict) for statement in
                               chosen_rule.feature_statements):
                            if concatenated_word not in words_instances:
                                words_instances[concatenated_word] = []
                            new_instance_features = dict(chosen_rule.variable.features)
                            for feature in new_instance_features:
                                new_instance_features[feature] = random.choice(feature.values)
                                for identity_statement in chosen_rule.identity_feature_statements:
                                    if identity_statement.contains(
                                            VariableFeatureDuo(variable=chosen_rule.variable, feature=feature)):
                                        other_features_in_statement = [f for f in identity_statement.features if
                                                                       f != feature]
                                        assert len(other_features_in_statement) > 0
                                        new_instance_features[feature] = combined_features_dict[
                                            other_features_in_statement[0]]
                            if new_instance_features not in words_instances[concatenated_word]:
                                words_instances[concatenated_word].append(new_instance_features)

        # thinning words_instances
        words_instances_thinned = {word: words_instances[word] for word in words_instances if
                                   random.random() < 0.8}  # TODO: decide upon this
        words_instances_thinned2 = {
            word: [instance for instance in words_instances_thinned[word] if random.random() < 0.8] for word in
            words_instances_thinned}
        for word in list(words_instances_thinned2):
            if len(words_instances_thinned2[word]) == 0:
                words_instances_thinned2.pop(word)
        words_instances = words_instances_thinned2

        self.variables[chosen_rule.variable.prototype] = [
            LexicalItem(word, chosen_rule.variable.prototype, words_instances[word]) for word in words_instances]

        self.variables.pop(v1.prototype)
        self.variables.pop(v2.prototype)

        self.variable_name_to_variable_map.pop(v1.prototype.name)
        self.variable_name_to_variable_map.pop(v2.prototype.name)

        self.rules.pop(v1.prototype)
        self.rules.pop(v2.prototype)

        self.rules[chosen_rule.variable.prototype].remove(chosen_rule)
        assert len(self.rules[chosen_rule.variable.prototype]) == 0

    def add_rule_below_word_level(self):
        lexical_variables_pool = [var for var in self.lexical_variables if
                                  len(var.lexical_items) > 0 and len(self.rules[var]) == 0]
        if len(lexical_variables_pool) == 0:
            raise NoProperLexicalVariablesError()

        chosen_var = random.choice(lexical_variables_pool)

        def split_word(word):
            num = random.randint(0, len(word))
            return word[:num], word[num:]

        items_split_tuples = [(item, *split_word(item.word)) for item in self.variables[chosen_var]]

        bases = {tup[1] for tup in items_split_tuples}

        suffixes = {tup[2] for tup in items_split_tuples}

        root_var_name = '{}-root'.format(chosen_var.name)
        root_var = LexicalVariable(root_var_name)
        self.variable_name_to_variable_map[root_var_name] = root_var
        self.variables[root_var] = [LexicalItem(base, root_var) for base in bases]
        self.rules[root_var] = []

        new_features = copy.deepcopy(chosen_var.features)

        static_old_features = list(chosen_var.features)
        static_new_features = list(new_features)
        features_map_old_to_new = {static_old_features[i]: static_new_features[i] for i in range(len(new_features))}

        suffix_new_features_map = {suffix: [] for suffix in suffixes}
        for item, base, suffix in items_split_tuples:
            for instance in item.instances:
                new_instance = {features_map_old_to_new[feature]: instance[feature] for feature in instance}
                if new_instance not in suffix_new_features_map[suffix]:
                    suffix_new_features_map[suffix].append(new_instance)

        agr_var_name = '{}-agr'.format(chosen_var.name)
        agr_var = LexicalVariable(agr_var_name, features=new_features)
        self.variable_name_to_variable_map[agr_var_name] = agr_var
        self.variables[agr_var] = [LexicalItem(suffix, agr_var, instances=suffix_new_features_map[suffix]) for suffix in
                                   suffix_new_features_map]
        self.rules[agr_var] = []

        self.variables[chosen_var] = []
        chosen_var.remove_all_lexical_items()

        var_prototype = chosen_var
        root_var_var_instance = VariableInstance(root_var)
        agr_var_var_instance = VariableInstance(agr_var)
        derivation = [root_var_var_instance, agr_var_var_instance]
        var_prototype_var_instance = VariableInstance(var_prototype)
        new_rule = LexicalRule(var_prototype_var_instance, derivation)
        self.rules[var_prototype].append(new_rule)

    def change(self):
        change_methods = [self.change_some_variable,
                          self.change_some_rule,
                          self.change_some_VI,
                          self.add_rule_below_word_level,
                          self.remove_rule_below_word_level]

        for i in range(NUM_TRIES):
            try:
                chosen_change_method = random.choice(change_methods)
                print('chose following change method: {}\n'.format(chosen_change_method.__name__))
                chosen_change_method()
            except MorphologicalLearnerError:
                print('\twas not successful\n')
                continue
            print('was successful!\n')
            return
        raise ChangingGrammarDidNotGoThroughError()

    def cky_parser(self, sentence):
        '''
            Parses the input string given the grammar, using the CKY algorithm.
            If the string has been generated by the grammar - returns a parse tree for the input string,
            otherwise - returns None.
            The CFG is given in CNF.
        '''

        def cky_parser_rec(words, start, length, results):
            if length <= 0 or length > len(words):
                return None

            if (start, length) in results.keys():
                return results[start, length]

            if length == 1:
                results[start, length] = set()
                for var in self.lexical_variables:
                    lis = [li for li in var.lexical_items if words[start] == li.word]
                    assert len(lis) <= 1
                    if len(lis) == 1:
                        results[start, length].add(Node(VariableInstance(var), [lis[0]]))

                return results[start, length]

            results[start, length] = set()
            for i in range(1, length):
                for left_subtree in cky_parser_rec(words,
                                                   start,
                                                   i,
                                                   results):
                    for right_subtree in cky_parser_rec(words,
                                                        start + i,
                                                        length - i,
                                                        results):
                        if left_subtree and right_subtree:
                            results[start, length].update(
                                {Node(VariableInstance(r.variable.prototype), [left_subtree, right_subtree], rule=r)
                                 for r in sum(self.rules.values(), [])
                                 if [v_d.prototype for v_d in r.derivation] == [left_subtree.key.prototype,
                                                                                right_subtree.key.prototype]})

            return results[start, length]

        if type(sentence) == str:
            sentence_words = sentence.split()
        else:  # type(sentence) == list:
            sentence_words = sentence
        nodes = {n for n in cky_parser_rec(sentence_words,
                                           0,
                                           len(sentence_words),
                                           {})  # for memoisation
                 if n.key.prototype == self.start_variable}
        if len(nodes) > 0:
            return Tree(next(iter(nodes)))  # TODO: change to generator when allowing multiple derivations
        return None

    def get_input_cost(self, sentences_literals):
        # sentences_literals = ['these dogs run', [(LexicalItem1, key1), (LexicalItem2, key2), ...] ]
        # sentences_literals = [
        #                       ('that cat runs', [('that' [{}], 'Det'), ('cat' [{}], 'N'), ('runs' [{}], 'V')]),
        #                       ('that cat walks', [('that' [{}], 'Det'), ('cat' [{}], 'N'), ('walks' [{}], 'V')]),
        #                       ('these cats run', [('these' [{}], 'Det'), ('cats' [{}], 'N'), ('run' [{}], 'V')]),
        #                        ...
        #                      ]
        all_succeeded = True
        all_costs = []
        for sentence, sentence_lst in sentences_literals:
            word_num_parts = [
                (li.word, self.max_num_leaves_under_var(self.variable_name_to_variable_map[var_name]))
                for li, var_name in sentence_lst]
            # word_num_parts = [('the', 2), ('cat', 3), ('walks', 1)]
            split_words = [(word, sum(
                (split_word_to_n_parts(word, cur_num_parts) for cur_num_parts in range(num_parts, num_parts + 1)), []))
                           for  # should be range(1, num_parts + 1)
                           word, num_parts in word_num_parts]
            # split_words = [(word, sum((split_word_to_n_parts(word, num_pts) for cur_num_pts in range(1, )), [])
            split_words_only = [word_options for word, word_options in split_words]
            split_words_only_in_lexicon = [[word for word in word_options if word in self.lexical_words] for
                                           word_options in split_words_only]
            # print("sentence={}\n".format(sentence))
            # print("split_words=\n{}".format(split_words))
            # print()
            # print("split_words_only=\n{}".format(split_words_only))
            # print(split_words_only)
            # print("____\n")

            options_cost_strs = []
            for option in itertools.product(*split_words_only):
                cur_sentence_parts = []
                for part in option:
                    cur_sentence_parts.extend(list(part) if type(part) == tuple else [part])
                cur_sentence = ' '.join(cur_sentence_parts)
                cur_tree = self.cky_parser(cur_sentence_parts)
                if cur_tree:
                    try:
                        cur_tree.apply_rules()

                        cur_leaves = cur_tree.get_leaves()

                        unresolved_features = [(ind, node, [(f, node.key.features[f]) for f in node.key.features if
                                                            type(node.key.features[f]) != int])
                                               for ind, node in enumerate(cur_leaves) if len(
                                [(f, node.key.features[f]) for f in node.key.features if
                                 type(node.key.features[f]) != int]) > 0]

                        flattened = [(ind, node, ft, ft_val)
                                     for ind, node, feature_lst in unresolved_features for ft, ft_val in feature_lst]
                        split_to_groups_for_id = {}
                        for ind, node, ftr, ftr_v in flattened:
                            if ftr_v not in split_to_groups_for_id:
                                split_to_groups_for_id[ftr_v] = []
                            split_to_groups_for_id[ftr_v].append((ind, node, ftr, ftr_v))
                        assert len(
                            [v for v in split_to_groups_for_id if type(v) == str and not v.startswith('Z')]) == 0 or \
                               (len([v for v in split_to_groups_for_id if
                                     type(v) == str and not v.startswith('Z')]) == 1 and '' in split_to_groups_for_id)
                        values_to_set = None
                        if '' in split_to_groups_for_id:
                            values_to_set = [[tpl] for tpl in split_to_groups_for_id.pop('')]
                        grouped = list(split_to_groups_for_id.values())
                        if values_to_set:
                            grouped.extend(values_to_set)

                        if len(grouped) == 0:
                            cost_str = ''.join(['11' + ''.join(['01' +
                                                                bin(list(leaf.key.features).index(featr))[2:].zfill(
                                                                    len(bin(len(leaf.key.features) - 1)[2:])) +
                                                                bin(leaf.key.features[featr])[2:].zfill(
                                                                    len(bin(featr.num_values - 1)[2:]))
                                                                for featr in leaf.features_to_code]) +
                                                ('10' + bin(
                                                    leaf.key.prototype.get_lexical_instances_by_features(
                                                        leaf.key.features).index(
                                                        (leaf.children[0].word, leaf.key.features)))[2:].zfill(
                                                    len(bin(len(
                                                        leaf.key.prototype.get_lexical_instances_by_features(
                                                            leaf.key.features)) - 1)[
                                                        2:])) if len(
                                                    leaf.key.prototype.get_lexical_instances_by_features(
                                                        leaf.key.features)) > 1 else '')
                                                for leaf in cur_leaves])
                            options_cost_strs.append(cost_str)

                        elif len(grouped) > 0:
                            assignments_costs = []

                            possible_assignments = itertools.product(*[ftr_lst[0][2].values for ftr_lst in grouped])
                            for assignment in possible_assignments:
                                for leaf in cur_leaves:
                                    leaf.features_to_code = {}

                                for ftr_lst_ind, ftr_lst in enumerate(grouped):
                                    for ind, node, ftr, ftr_v in ftr_lst:
                                        node.key.set_feature_value(ftr, assignment[ftr_lst_ind])

                                        assert ftr not in node.features_to_code
                                        node.features_to_code[ftr] = assignment[ftr_lst_ind]

                                relevant_instances_for_cur_leaves = [[(word, features) for word, features in
                                                                      leaf.key.prototype.get_lexical_instances_by_features(
                                                                          leaf.key.features)
                                                                      if word == leaf.children[0].word] for leaf in
                                                                     cur_leaves]
                                if all(len(relevant_instances_lst) > 0 for relevant_instances_lst in
                                       relevant_instances_for_cur_leaves):

                                    # removing redundant features from being coded into cost_str
                                    # for leaf in cur_leaves:
                                    #     lex_insts = leaf.key.prototype.get_lexical_instances_by_features(leaf.key.features)
                                    #     ftrs_dict_copy = copy.copy(leaf.key.features)
                                    #     assert all(type(leaf.key.features[ftr]) == int for ftr in leaf.key.features)
                                    #     for _ in range(len(leaf.key.features)):
                                    #         for feature in [ftr for ftr in leaf.key.features if
                                    #                         ftr in leaf.features_to_code]:
                                    #             if feature in ftrs_dict_copy:
                                    #                 ftrs_dict_copy2 = copy.copy(ftrs_dict_copy)
                                    #                 ftrs_dict_copy2.pop(feature)
                                    #                 cur_lex_insts = leaf.key.prototype.get_lexical_instances_by_features(
                                    #                     ftrs_dict_copy2)
                                    #                 if cur_lex_insts == lex_insts:
                                    #                     ftrs_dict_copy = ftrs_dict_copy2
                                    #                     assert feature in leaf.features_to_code
                                    #                     leaf.features_to_code.pop(feature)
                                    for v in [k for k in split_to_groups_for_id if k.startswith('Z')]:
                                        lst_featrs = split_to_groups_for_id[v]
                                        lst_featrs_copy = copy.copy(lst_featrs)
                                        lst_featrs_copy.sort(key=lambda x: len(x[1].key.features))
                                        for iii, (ind, node, ftr, ftr_v) in enumerate(lst_featrs_copy):
                                            if iii == 0:
                                                continue
                                            node.features_to_code.pop(ftr)

                                    cost_str = ''.join(['11' + ''.join(['01' +
                                                                        bin(list(leaf.key.features).index(featr))[
                                                                        2:].zfill(
                                                                            len(bin(len(leaf.key.features) - 1)[2:])) +
                                                                        bin(leaf.key.features[featr])[2:].zfill(
                                                                            len(bin(featr.num_values - 1)[2:]))
                                                                        for featr in leaf.features_to_code]) +
                                                        ('10' + bin(
                                                            leaf.key.prototype.get_lexical_instances_by_features(
                                                                leaf.key.features).index(
                                                                (leaf.children[0].word, leaf.key.features)))[2:].zfill(
                                                            len(bin(len(
                                                                leaf.key.prototype.get_lexical_instances_by_features(
                                                                    leaf.key.features)) - 1)[
                                                                2:])) if len(
                                                            leaf.key.prototype.get_lexical_instances_by_features(
                                                                leaf.key.features)) > 1 else '')
                                                        for leaf in cur_leaves])
                                    assignments_costs.append(cost_str)

                            if len(assignments_costs) > 0:
                                minimal_cost = min(assignments_costs, key=lambda s: len(s))

                                options_cost_strs.append(minimal_cost)

                    except SetFeatureValueStatementsValuesClashError as e:
                        print('Could not apply clashing set rules, moving on to next option')
                    except Exception as e:
                        raise e

            if len(options_cost_strs) > 0:
                minimal_cost_total = min(options_cost_strs, key=lambda s: len(s))
                all_costs.append(minimal_cost_total)

            else:
                print('could not find tree for sentence: {}'.format(sentence))
                all_succeeded = False
                break

        if all_succeeded:
            print('Sentences costs:\n{}\n'.format(all_costs))
            d_g_cost = sum([len(cost) for cost in all_costs])
            print('G: {}, D|G: {} \t---> G + D|G: {}'.format(self.cost, d_g_cost, self.cost + d_g_cost))
            return d_g_cost
        else:
            raise ImproperNeighbourError()

    def get_energy(self, sentences_literals):
        return self.cost + 50 * self.get_input_cost(sentences_literals)

    def get_neighbour(self):
        neighbour = copy.deepcopy(self)
        neighbour.change()
        return neighbour
